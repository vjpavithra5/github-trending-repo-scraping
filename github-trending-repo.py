
import requests
import pandas as pd
from bs4 import BeautifulSoup


URL = 'https://github.com/trending'
response = requests.get(URL)
page = BeautifulSoup(response.content, 'html.parser')



repos = {'user':[], 'repo':[], 'description':[],'total-stars':[], 'url':[]}
def get_trending_repos():
    box = page.find_all('article', class_="Box-row")
    for b in box:
        try:
            title = b.find('h1').text.strip().replace("\n","") 
            repos['user'].append(title.split()[0])
            repos['repo'].append(title.split()[2])
        except:
            repos['user'].append(" ")
            repos['repo'].append(" ")
        try:
            repos['description'].append(b.find('p').text.strip())
        except:
            repos['description'].append(" ")
        try:
            repos['total-stars'].append(page.find('a', class_='mr-3').text.strip())
        except:
            repos['total-stars'].append(" ")
        try:
            repos['url'].append(URL+b.find('h1').findChildren('a')[0]['href'])
        except:
            repos['url'].append(" ")
    return



get_trending_repos()

repo_df = pd.DataFrame(repos)
repo_df.to_csv('trending-repo.csv', index=False)

